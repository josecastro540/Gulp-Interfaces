
let gulp = require('gulp');
let sass = require ('gulp-sass');
let gls = require('gulp-live-server');

gulp.task('mover',()=>{
    gulp.src('node_modules/bootstrap/dist/css/bootstrap.min.css')
    .pipe(gulp.dest('css'))    
})

gulp.task('sass',()=>{
  return  gulp.src('sass/*.sass')
  .pipe(sass().on('error', sass.logError))
  .pipe(gulp.dest('css'))
})

gulp.task('sass:watch',()=>{
    gulp.watch('sass/*.sass', ['sass'])
})

gulp.task('serve', ()=>{
    let server = gls.static('./', 3700);
    server.start();

    gulp.watch(['css/*.css', './*.html'], function (file) {
        server.notify.apply(server, [file]);
      });

})

gulp.task('default', ['mover', 'sass:watch', 'serve'])